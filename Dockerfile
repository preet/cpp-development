FROM ubuntu:22.04

ENV DEBIAN_FRONTEND=noninteractive

ARG SCONS_ARGS="-j $(cat /proc/cpuinfo | grep processor | wc -l)"

RUN apt update && apt install -y \
  scons=4.0.1+dfsg-2 \
  build-essential \
  gdb

WORKDIR /root/cpp-development
VOLUME ["/root/cpp-development"]

CMD ["/bin/bash", "-c", "scons ${SCONS_ARGS}"]
