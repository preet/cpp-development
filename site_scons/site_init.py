"""
SCons site init - executes before SConstruct and SConscripts
"""

import cli
from SCons.Script import *

cli.init()
