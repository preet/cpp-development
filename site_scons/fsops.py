import fnmatch
import os
from pathlib import Path
from typing import List, Set

from SCons.Script import *


def find_c_sources(search_dirpath: Path) -> List[File]:
    source_patterns = ["*.c", "*.cpp", "*.cc"]
    include_patterns = ["*.h", "*.hpp", "*.hh"]
    discovered_sources: Set[File] = set()

    root_dirpath = os.path.relpath(str(search_dirpath))
    for dirpath, _, filenames in os.walk(root_dirpath):
        for source_pattern in source_patterns:
            matching_source_filenodes = Glob(os.path.join(dirpath, source_pattern))
            for filenode in matching_source_filenodes:
                discovered_sources.add(filenode)

    return list(discovered_sources)


def find_c_include_paths(search_dirpath: Path) -> List[Dir]:
    include_patterns = ["*.h", "*.hpp", "*.hh"]
    discovered_include_paths: Set[Dir] = set()

    root_dirpath = os.path.relpath(str(search_dirpath))
    for dirpath, _, filenames in os.walk(root_dirpath):
        for include_pattern in include_patterns:
            if len(fnmatch.filter(filenames, include_pattern)) > 0:
                discovered_include_paths.add(Dir(dirpath))

    return list(discovered_include_paths)
