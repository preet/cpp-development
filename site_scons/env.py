"""
References:
- https://scons.org/doc/production/HTML/scons-user/apa.html
"""

import os

from SCons.Script import *

verbose = GetOption("verbose")

env = Environment(
    ENV=os.environ,
    tools=[
        "mingw",
    ],
    CFLAGS=[
        "-std=c99",
    ],
    CXXFLAGS=[
        "-std=c++17",
    ],
    CCFLAGS=[
        "-g",
    ],
    ASFLAGS=[],
    LINKFLAGS=[],
    CPPPATH=[],
    CPPDEFINES=[],
    LIBS=[],
    LIBPATH=[],
)


if not verbose:
    env["CCCOMSTR"] = "Compiling $SOURCE"
    env["CXXCOMSTR"] = "Compiling $SOURCE"
    env["LINKCOMSTR"] = "Linking $TARGET"
