#!/usr/bin/env bash

set -x

subcommand=$1
target=$2

self_dirpath=$(dirname ${BASH_SOURCE:-$0})
docker_name=cpp-development
run_command=

export DOCKER_CLI_HINTS=false

if test "$subcommand" = "setup"; then
  docker build -t $docker_name $self_dirpath
elif test "$subcommand" = "shell"; then
  run_command="/bin/bash"
elif test "$subcommand" = "build"; then
  run_command="scons"
else
  : # Run docker; passthrough command line arguments
fi

docker run --rm -it --mount src=.,target=/root/cpp-development,type=bind --name=$docker_name $docker_name $run_command ${@:3:}
